import React from 'react';
import './App.css';
import Result from "./Result.js"

class App extends React.Component {

  state={
    text:"Désolé, nous n'avons pas encore analysé cette application"
    ,ecriture:""
  };

  eventWrite=(e)=>{
    this.setState({
      ecriture:e.target.value
    })
  }

  render(){ return (
    <div className="App">
      <header className="App-header">
      <p>
          Test de vitesse d'écriture 
        </p>
        <img src={"/FeedisRecherche.png"} className="App-logo" alt="logo" />
        <div>
      <div class="form-group"> 
         <input type="text" class="form-control"   onChange={(e)=>{this.eventWrite(e)}}   placeholder={this.state.text}/>
         <small  class="form-text text-muted">Ce test est dans le but projet React(Enzyme analyse) <br/> Vert = Correct</small>
         <Result text={this.state.text} ecriture={this.state.ecriture}/>
      </div>
      </div>
      </header>
    </div>
  );
} }
 

export default App;
