import React from 'react'
import Enzyme,{shallow} from 'enzyme'
import Result from './Result'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({adapter:new Adapter() })

describe('Result',()=>{
 
    
it('Verification Resultat',()=>{
      const rep=shallow(<Result text="test" ecriture="test"/>);
      const spanResult = rep.find('span')
      expect(spanResult.text()).toBe("Votre Réponse");
      });

it('Verification ClassName success',()=>{
        const rep=shallow(<Result text="1" ecriture="1"/>);
        const spanResult = rep.find('span')
        expect(spanResult.hasClass('badge-success')).toBe(true);
        });


it('Verification ClassName danger',()=>{
    const rep=shallow(<Result text="Notest" ecriture="test"/>);
    const spanResult = rep.find('span')
    expect(spanResult.hasClass('badge-danger')).toBe(true);
    });

 

});