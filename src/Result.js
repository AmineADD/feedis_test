import React from 'react';
 
function Result(propos){  
  return (
    <span  className={"badge badge-"+(propos.text===propos.ecriture ? "success":"danger")}>Votre Réponse</span>
  );
}  
 

export default Result;
